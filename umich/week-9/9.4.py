'''
 Program reads through the mbox-short.txt 
 Print: who has the sent the greatest number of mail messages. 
 The program looks for 'From ' lines and takes the second word
 of those lines as the person who sent the mail. 
 The program creates a Python dictionary that maps the sender's 
 mail address to a count of the number of times they appear in the file.
 After the dictionary is produced, the program reads
 through the dictionary using a maximum loop to find the most prolific sender
'''


name = raw_input('Enter file name: ')
if len(name) < 1 : name = "mbox-short.txt"


fhand = open(name)
c = dict()
for line in fhand:
    if not line.startswith('From ') : continue
    pieces = line.split()
    email = pieces[1]
    c[email] = c.get(email,0) + 1

#print c.items()


maxval = None
maxkee = None

for kee,val in c.items() :
    if maxval == None or maxval < val :
        maxval = val
        maxkee = kee
# print kee, val, maxkee, maxval
print maxkee, maxval
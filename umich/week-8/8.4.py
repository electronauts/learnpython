# 1. Read file line by line.  
# 2. Split each line into a list of words - split() function.
# 3. Program should build a list of words. 
# 4. For each word on each line check to see if the word is already in the list
# 5. and if not append it to the list. When the program completes, sort and print the resulting words in alphabetical order.
#You can download the sample data at http://www.pythonlearn.com/code/romeo.txt

words = []
n = '0'

fname = raw_input("Enter file name: ")
if len(fname) == 0:              # write this if we want to skip writing filename eachtime
	fname = 'romeo.txt'
fhand = open(fname)
for line in fhand:
    line = line.rstrip()
#    print line
    words = words + line.split()



words.sort()

# This function removes duplicates from a list and preserves order
def f2(words): 
   # order preserving
   checked = []
   for e in words:
       if e not in checked:
           checked.append(e)
   return checked

print f2(words)

# Write a program that prompts for a file name, then opens that file and reads through the file, looking for lines of the form:
#   X-DSPAM-Confidence:    0.8475
#Count these lines and extract the floating point values from each of the lines and compute the average of those values and produce an output as shown below.
# sample data http://www.pythonlearn.com/code/mbox-short.txt.
y = 0
num = 0
total = 0

# Use the file name mbox-short.txt as the file name
fname = raw_input("Enter file name: ")
if len(fname) == 0:
	fname = 'mbox-short.txt'
fh = open(fname)
for line in fh:
    if not line.startswith("X-DSPAM-Confidence:") : continue

    y = y + 1
    x = line
 #   print x
    # Find where is this column end. Print position
    pos = x.find(':')
#    print x[pos+1:]
    num = float(x[pos+1:])
    total = total + num
    average = total/y 
print "Average spam confidence:", average
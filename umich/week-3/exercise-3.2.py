# Rewrite Exercise 3.1 so the program should handle non-numeric input gracefully

# Try and Except 

hours = raw_input('Enter Hours: ')
rate = raw_input('Enter Rate: ')

try:
    hours = float(hours)
    rate = float(rate)
    overtime = hours - 40.0
    pay1 = (40 + overtime * 1.5) * rate
    pay2 = hours * rate

    if hours > 40:
	    print 'We worked MORE than 40 hours this week and earned', pay1, '$.'
    else:
	    print 'We earned:', pay2, '$'

except:
	print 'Error, please enter numeric input'








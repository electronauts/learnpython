# Rewrite your pay computation to give the employee 1.5 times the hourly rate for hours worked above 40 hours.

hours = raw_input('Enter Hours: ')
rate = raw_input('Enter Rate: ') # How much we pay, hourly

#  convert the data you get from console to integers or we get an error. float - to work with point. "."
hours = float(hours)
rate = float(rate)


if hours > 40:
	overtime = hours - 40
	pay = (40 + overtime * 1.5) * rate
	print 'We worked MORE than 40 hours this week and earned', pay, '$.'
else:
	pay = hours * rate
	print 'We worked LESS then 40 hours this week and earned', pay, '$'
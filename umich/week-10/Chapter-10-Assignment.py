'''
10.2 Write a program to read through the mbox-short.txt and figure out the distribution by hour of the day for each of the messages. You can pull the hour out from the 'From ' line by finding the time and then splitting the string a second time using a colon.
                         1        2   3   4    5       6
#From stephen.marquard@uct.ac.za Sat Jan  5 09:14:16 2008

Once you have accumulated the counts for each hour, print out the counts, sorted by hour as shown below. Note that the autograder does not have support for the sorted() function.

Desired output:

04 3
06 1
07 1
09 2
10 3
11 6
14 1
15 2
16 4
17 2
18 1+
19 1


'''
count = 0
a_split = list()

fname = raw_input("Enter file: ")
if len(fname) < 1 : fname = "mbox-short.txt"
fhand = open(fname)
c = dict()

for line in fhand:
    if not line.startswith('From ') : continue
    pieces = line.split()
    time = pieces[5]
    parts = time.split(':')
    hour = parts[0]
    c[hour] = c.get(hour,0) + 1


lst = list()
for key in c:
    value = c[key]
    lst.append( (value, key) ) 

lst.sort(reverse=True)

#for value, key in lst:
#  print key, value



flipped = list() # give me an empty list


for kie, vaal in c.items(): # its two iteration variables - these will iterate through each tuple this which iterate through each tuple
    # print kie, vaal # sanity check
    newtup = (kie, vaal)
#    print newtup
    flipped.append(newtup)


# print flipped  # print the whole list
flipped.sort() # sorting is done by repeating ... The big numbers at the beginning
# print flipped  # print list

for key, vall in flipped[:10] : # iterate through the first 10
	print key, vall
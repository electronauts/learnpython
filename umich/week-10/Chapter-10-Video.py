
'''
Chapter 10. Tuples
 
Task: top 10 most common words

Structure:

6. Sort by the value instead of the key: 
    - make a list 
    - loop through the results of the dictionary
    - make a new Tuple. Instead is of the key value, is what you get from items We gonna do value, key
8. Sort that in reverse order
9. Print the first 10 elements of that sorted in reverse order

'''
fhand = open('romeo.txt')  # 1. Read the file 
counts = dict()            # 2. Make an empty dictionary 
for line in fhand:         # 3. Read the file line by line
	words = line.split()   # 4. Split each line

# 5. Loop through the line using the dictionary pattern from the dictionaries Chapter (using get pattern - access/initialize new words we haven't seen)
	for word in words:
		wrd = word.lower() # Mapping into lower case
		counts[wrd] = counts.get(wrd, 0 ) + 1

# print counts             # print dictionary 
print counts.items()       # print a list of 2-item tuples - key-value pairs.
#-----------------------------------




flipped = list() # give me an empty list

# 
for kie, vaal in counts.items(): # its two iteration variables - these will iterate through each tuple this which iterate through each tuple
    # print kie, vaal # sanity check
    newtup = (vaal, kie)
    print newtup
    flipped.append(newtup)


print flipped  # print the whole list
flipped.sort(reverse=True) # sorting is done by repeating ... The big numbers at the beginning
print flipped  # print list

for key, vall in flipped[:10] : # iterate through the first 10
	print key, vall